import guitar.Guitar;
import org.junit.Test;
import players.*;

public class TestGuitarPlayer {

    @Test
    public void testGuitarPlayer(){
        Guitar guitar = new Guitar("Yamaha", "Oak");

        Player guitarPlayer  = new Player();
        guitarPlayer.takeCommand(new AStringPlayer(guitar));
        guitarPlayer.takeCommand(new GStringPlayer(guitar));
        guitarPlayer.takeCommand(new BStringPlayer(guitar));
        guitarPlayer.takeCommand(new HigherEStringPlayer(guitar));
        guitarPlayer.takeCommand(new GStringPlayer(guitar));

        guitarPlayer.executeMelody();
    }
}
