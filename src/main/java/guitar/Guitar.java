package guitar;

public class Guitar {
    private String name;
    private String wood;

    public Guitar(String name, String wood) {
        this.name = name;
        this.wood = wood;
    }

    public void playE(){
        System.out.println("E string");
    }

    public void playA(){
        System.out.println("A string");
    }

    public void playD(){
        System.out.println("D string");
    }

    public void playG(){
        System.out.println("G string");
    }

    public void playB(){
        System.out.println("B string");
    }

    public void playHigherE(){
        System.out.println("e string");
    }
}
