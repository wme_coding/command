package players;

import guitar.Guitar;

public class BStringPlayer extends StringPlayer {
    public BStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playB();
    }
}
