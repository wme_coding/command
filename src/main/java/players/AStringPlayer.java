package players;

import guitar.Guitar;

public class AStringPlayer extends StringPlayer {
    public AStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playA();
    }
}
