package players;

import guitar.Guitar;

public class GStringPlayer extends StringPlayer {
    public GStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playG();
    }
}
