package players;

import guitar.Guitar;

public class EStringPlayer extends StringPlayer {
    public EStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playE();
    }
}
