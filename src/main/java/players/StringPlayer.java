package players;

import command.Command;
import guitar.Guitar;

public abstract class StringPlayer implements Command {
    protected Guitar guitar;

    public StringPlayer(Guitar guitar) {
        this.guitar = guitar;
    }

    public abstract void playString();
}
