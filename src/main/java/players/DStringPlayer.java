package players;

import guitar.Guitar;

public class DStringPlayer extends StringPlayer {
    public DStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playD();
    }
}
