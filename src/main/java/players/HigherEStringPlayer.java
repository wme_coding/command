package players;

import guitar.Guitar;

public class HigherEStringPlayer extends StringPlayer {
    public HigherEStringPlayer(Guitar guitar) {
        super(guitar);
    }

    public void playString() {
        guitar.playHigherE();
    }
}
