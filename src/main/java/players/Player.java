package players;

import command.Command;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private List<Command> commands;

    public Player() {
        this.commands = new ArrayList<>();
    }

    public void takeCommand(Command command){
        commands.add(command);
    }

    public void executeMelody(){
        for(Command c: commands){
            c.playString();
        }

        commands.clear();
    }
}
